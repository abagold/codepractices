# ABAGOLD CODE PRACTICES

## for code development:

Adopt Git Flow as the branching model for production software. [Read Here](./pages/gitflow.md)

Adopt a naming convention for codebase.  [Read Here](./pages/naming.md)

Use a Dockerfile to define the Docker image for the software.

Use Docker Compose to define the multi-container architecture for application, including dependencies such as databases.

Use environment variables to configure Docker containers at runtime, such as database connection strings, API keys, and log levels. Environment should not
be committed on the repository but the code should contain a env template file.


## for code design:

Use the Soft Delete pattern in databases. This pattern involves using a separate column to mark deleted rows instead of physically deleting data from the database. This makes it easy to recover deleted data in case of errors.

Implement REST APIs with the OpenAPI specification. This enables clear and precise definition of API structure and parameters, simplifying documentation and interaction between software components.

Write tests is not really efficient. We should have small tests to verify the software is not crashing and have unit tests for sensible features.

Linters should be implemented at the beginning of the project.

Paged APIs should follow this [rules](./pages/api_response.md).
# JWT Tokens

We have a tool for build a custom JWT [here](https://abaplayground.surge.sh/jwt.html), the tool is pure html and is avaiable in
[here](./examples/jwt.html).

The structure of the JWT will change over time but the base skelethon is:

HEADER
```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

PAYLOAD
```json
{
  "sub": "1234567890",
  "name": "Scrooge McDuck",
  "iss": "abacom.usersmanager",
  "exp": 1687334092
}
```
Where:
- **sub**: is the subject (or user id)
- **name**: human friendly name
- **iss**: issuer of the token
- **exp**: expiring date of the token