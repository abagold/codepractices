# Gitflow Workflow

The Gitflow Workflow defines a strict branching model to ensure the stability of different environments and the consistency of feature development.

Remember that it's crucial to perform merges correctly, to keep history consistent, avoid potential conflicts, and keep the codebase stable. 

## Main Branches

At the core, the development model is greatly similar to other popular models, and revolves around two main branches with an infinite lifetime:

1. **`main`**: This is the production branch, and the source code in this branch always reflects a production-ready state.
2. **`develop`**: This is the branch for integrating different features ready for the next release. The source code in this branch reflects the latest integrated development changes for the next release.

## Supporting Branches

Next, it employs a variety of supporting branches to aid parallel development between team members, ease tracking of features, prepare for production releases, and assist in quickly fixing live production problems. Unlike the main branches, these branches always have a limited life time, and are removed eventually. These branches include:

- **Feature branches**: Branch off from `develop` and merge back into `develop`.
    - Naming convention: anything except `main`, `develop`, `release-*`, or `hotfix-*`.

- **Release branches**: Branch off from `develop` and merge back into `main` and `develop` when done.
    - Naming convention: `release-*`.

- **Hotfix branches**: Branch off from `main` and merge back into `main` and `develop`.
    - Naming convention: `hotfix-*`.

## Typical Flow

Here's a typical workflow:

1. A `feature` branch is created from `develop` when new feature work begins.
2. When development of the feature is complete, it is merged back into the `develop` branch.
3. When it's time to prepare for a release, a `release` branch is created off `develop`.
4. Any bugs found in the `release` branch are fixed in it.
5. When the `release` branch is ready, it's merged into `main` and `develop`. The commit on `main` is tagged with the version number.
6. If a bug is found on `main`, a `hotfix` branch is created off `main`. When the bug is fixed, the `hotfix` branch is merged back into both `main` and `develop`.

By using this workflow, we ensure that the `main` branch always reflects the production-ready state and `develop` serves as an integration branch for features. Plus, it gives every team member a clear project structure which is easy to follow and understand.

## Commit Message Convention

For commit messages, we adopt the [Conventional Commits](https://www.conventionalcommits.org/) specification. Conventional Commits is a lightweight convention on top of commit messages. It provides an easy set of rules for creating an explicit commit history, which makes writing automated release notes, versioning, and navigating through the commit log much easier.


