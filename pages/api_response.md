# Best Practices for Serving API Responses


## Paged Endpoints

### Introduction

When designing APIs that handle paged endpoints, it is crucial to provide a well-structured and consistent response format.

A short example of a well formatted JSON response is:

```js
{
  "data": [...], // Array of objects, the actual payload
  "metadata": {
    "totalItems": 100, // total items founded
    "itemsPerPage": 10, // items per page 
    "currentPage": 3, // current page
    "totalPages": 10 // total pages
  },
  "links": {
    "first": "/api/resource?pageNumber=1&pageSize=10", // Link to the first page
    "prev": "/api/resource?pageNumber=2&pageSize=10", // Link to the next page
    "next": "/api/resource?pageNumber=4&pageSize=10", // Link to the prev page
    "last": "/api/resource?pageNumber=10&pageSize=10" // Link to last page
  }
}
```

### Request

In order to query a paginated endpoint, you can use the following parameters as query strings in the URL path:

- `pageNumber`: Specifies the desired page number of the paginated data.
- `pageSize`: Determines the number of items per page.
- `sort`: Allows sorting the data based on a specific field and sort. For example, `sort=surname:asc` would sort the data by the "surname" field in ascending sort. This keyword can accept many input comma separated (eg. `sort=surname:asc,name:desc`). sort is optional, can be `asc` or `desc`, if omitted the default is `asc`.

First page is `1`.

### Body

#### Data Representation

The core component of the response payload is the `data` field, which represents the actual paginated data. This field typically contains an array of items or objects specific to the endpoint's purpose. The structure and content of this field depend on the application's data model and the specific endpoint requirements.

#### Metadata

To provide additional context and information about the paginated data, including the number of total items, items per page, current page, and total pages, it is essential to include a `metadata` section in the API response. This metadata allows clients to understand the pagination details and navigate through the available data effectively.

#### Links

To enable easy navigation between pages, it is recommended to include a `links` section in the API response. This section should contain links to the first, previous, next, and last pages of the paginated resource. These links provide clients with convenient navigation options, allowing them to retrieve the desired subset of data efficiently.

#### Exception Handling

In cases where an exception occurs during the processing of the API request, it is essential to handle and communicate the exception to the client effectively. The API response can include an `exception` field that contains relevant information about the error. This field can include details such as an error code, a descriptive message, and any additional error-specific data. By including exception handling in the response structure, you provide clients with meaningful error information, enabling them to handle exceptional cases gracefully.

