# Naming Conventions

Effective and consistent naming conventions in software development projects are crucial for maintaining readability and manageability of the code. Here are the naming conventions that we use across different components:

## Code

When naming variables and classes in the code, we adhere to the following conventions:

- **Variables**: We use camelCase for naming variables. An example would be `myVariableName`.
  
- **Classes or Controllers**: For classes and controllers, we use PascalCase. An example would be `MyClassName`.

## Database

Naming conventions for database tables (or collections) and columns (or fields) are as follows:

- **Tables or Collections**: We use plural names and lowercase letters. An example would be `users`.

- **Columns or Fields**: Names are in lowercase and separated by underscores if multi-word. An example would be `user_id`.

## HTTP / API 

For HTTP or API related naming, we follow these conventions:

- **API URLs**: We use lowercase letters, and if a separator is needed, we use a hyphen ('-'). An example would be `/api/users/user-id`.

- **JSON**: We use camelCase for the names of properties in a JSON object. An example would be `{ "firstName": "John", "lastName": "Doe" }`.

By following these naming conventions, we can ensure that our codebase remains consistent and easy to navigate for all developers. They provide a shared language that everyone can understand, which improves communication and reduces confusion.
